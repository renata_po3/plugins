HEADERS += ../tetris/interface.h \
    mode/mode.h

SOURCES += \
    mode2/mode2.cpp

TEMPLATE = lib
CONFIG += plugin

DISTFILES += \
    mode/Interface.json
