HEADERS += ../tetris/interface.h \
    mode/mode.h

SOURCES += \
    mode1/mode1.cpp

TEMPLATE = lib
CONFIG += plugin

DISTFILES += \
    mode/Interface.json
