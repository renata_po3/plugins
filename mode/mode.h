#ifndef MODE_H
#define MODE_H

#include "../tetris/interface.h"

class mode : public QObject, public Interface
{
    Q_OBJECT
    Q_INTERFACES(Interface)
    Q_PLUGIN_METADATA(IID "Tetris.Interface" FILE "Interface.json")

public:
    virtual ~mode(){}
    virtual QString pluginName();
    virtual QPalette changeView();

};
#endif // MODE_H
