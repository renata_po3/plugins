#include "mode/mode.h"

QString mode::pluginName()
{
    return "mode 2";
}

QPalette mode::changeView()
{
    QPalette palette;
    palette.setColor(QPalette::Window, QColor(Qt::black));
    palette.setBrush(QPalette::ColorRole::WindowText, QBrush(Qt::red));
    return palette;
}
