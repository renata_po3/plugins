#include "mode/mode.h"

QString mode::pluginName()
{
    return "mode 3";
}

QPalette mode::changeView()
{
    QPalette palette;
    palette.setColor(QPalette::Window, QColor(Qt::darkBlue));
    palette.setBrush(QPalette::ColorRole::WindowText, QBrush(Qt::yellow));
    return palette;
}
